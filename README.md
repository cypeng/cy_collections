# MY COLLECTIONS
<!-- blank line -->
<dl>
　<dd>C.Y. Peng</dd>
<!-- blank line --> 
  <dd>Last Update 20210905 </dd>
</dl> 

## My Kaggle Projects, [User Name: Yeh, Old User Name: Жапырақ Batyr](https://www.kaggle.com/pcyslm)
* Tabular Playground Series, Top 10, Jan 2021-present, New Topic per Month
  * Including various target for different dataset, such as regression, multi-classification and so on.
  * Established denoise autoencode (DAE), transformer<sup>*1</sup>  + DAE<sup>*2</sup> , xgb, lgbm, cb to slove the problem from training/testing/validation.
  * Study on the different escemble model to increase the generation of the model.
<!-- blank line -->

* Natural Language Processing with Disaster Tweets, Top 8, Aug. 2020 -present
  * Basic NLP features engineering for model established.
  * Established the LSTM, Bi-LSTM from trainin/testing/validation to predict sentiment classification.
  * Build the pretraining BERT<sup>*3</sup> model and fine tunning to predict the sentiment classification.
<!-- blank line -->

* Riiid Answer Correctness Prediction, top 24, Sep. 2020-Jan. 2021
  * Features engineering for statistics of the data variables.
  * Established the LGBM model training/testing/validation. 
  * Escemble model with other reference model, such as SAKT<sup>*4</sup>.
<!-- blank line -->

* [Tweet Sentiment Extraction, Apr. 2020 - June 2020](https://www.kaggle.com/pcyslm/bi-lstm-glove-preprocessing-lb-0-605?scriptVersionId=35209723 )
  * Basic NLP features engineering for model established.
  * Established the LSTM, Bi-LSTM from trainin/testing/validation to predict text selection.
  * Build pretraining BERT<sup>*3</sup> model and fine tunning to predict the text selection.
<!-- blank line -->

* [Kaggle Ver.] [Digit MNIST Recognition, top 5, Aug. 2019 -present](https://www.kaggle.com/pcyslm/basic-model-for-mnist-solution-tf-v1-top-10) & [Gitlab Ver.] [LeNet-5 Model in TensorFlow for MNIST Dataset Digit Recognizer, accuracy 0.9](https://gitlab.com/cypeng/cy_tf_lenet) 
  * Data augmentation for MNIST dataset.
  * Established the LeNet-5 & VGG-like model by using TensorFlow framework to model training/testing/validation for MNIST handwriting datasets recognition.
<!-- blank line -->

* [Gitlab Ver.] [Titanic: Machine Learning from Disaster, Accuracy: 0.80, top 2, Aug. 2019-present](https://gitlab.com/cypeng/cy_titanic_ml_for_disaster)
  * Use machine learning methodology to create a model that predicts which passengers survived the Titanic shipwreck under data science analysis flow from explore data analysis to model selection. 
  * Features analysis by decision stump and PCA; model established such as random forest, decision tree, xgb and so on.
<!-- blank line -->

* [House Price Prediction, top 7, Aug. 2019 -2020]
  * Build model to predict the house prices from features.
<!-- blank line -->

## My Gitlab Projects, [User Name: cypeng](https://gitlab.com/cypeng/cy_collections)
* [Case Study: Bank Marketing Analysis for Imbalance Data, UCI Machine Learning Repository, F1 Score 0.87, Jul. 2019-Present](https://gitlab.com/cypeng/cy_bank_marketing_analysis).<sup>*5</sup> 
  * This research focus on the minority of the people who order the product for this topic.
  * Established the binary classification system to predict the client required if the bank product order. 
<!-- blank line -->

* [Yolo-v3 (CY ver) Model in TensorFlow for Objects Recognition, F1 Score 0.8, Apr. 2019-Present](https://gitlab.com/cypeng/cy_tf_yolo)<sup>*6-*8</sup>. 
  * Data augmentation for dataset training.
  * Reconstructed the CNN Yolo-v3 from recovering the official pretraining model to the transfer learning for the model training/testing/validation for custom objects recognition. 
<!-- blank line -->

* [Finance Manager: Fund Data Web Scraping, Analysis and ML Prediction Algorithm, R2 Score 0.90, Dec. 2018 - Present](https://gitlab.com/cypeng/cy_finance_manager).
  * Using the fund data from the web scraping to established the model to predict the net asset value of the fund from the data clean, data preprocessing, model established and model selection.
  * Established the time series model, such as SARIMA, and Facebook prophet time series model<sup>*9</sup> to predict the value of the fund. 
<!-- blank line -->

* [My LeetCode Solutions List, Solved: 207/2342, Easy: 81, Medium: 120, Hard: 6](https://gitlab.com/cypeng/cy_my_leetcode_solutions_list). 
  * Synchronizing my LeetCode solutions list scraping 
  * User: pcysl 
<!-- blank line -->

## Reference
* *1 Ashish Vaswani, Noam Shazeer, Niki Parmr, Jakob Uszkoreit, Llion Jories, Aidan N. Geomez, Lukasz Kaiser and Illia Polosukhin, "Attention is All You Need, " Computation and Language, 2017.
* *2 Pascal Vincent, Hugo Larochelle, Isabelle Lajoie, Yoshua Bengio and Pierre-Antoine Manzagol, "Stacked Denoising Autoencoders: Learning Useful Representations in a Deep Network with a Local Denoising Criterion," in Journal of Machine Learning Research, 2010.
* *3 Jacob Devlin, Ming-Wei Chang, Kenton Lee and Kristina Toutanova, "BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding, " Tech report, Google AI, 2019.
* *4 Shalini Pandey and George Karypis, "A Self-Attentive model for Knowledge Tracing, " Proceedings of The 12th International Conference on Educational Data Mining, 2019.
* *5 Sergio Moro, Raul M.S. Laureano and Paulo Cortezo, "Using Data Mining for Bank Direct Marketing: an Application of the Crisp dm Methodology, " European Simulation and Modelling Conference, October 2011.
(For bank, there are two types sampling data, imbalance data and balance data. In *5, use the balance sampling data to build the baseline model.)
* *6 Joeph Redmon and Ali Farhadi, "YOLOv3: An Incremental Improvement, " Tech report, 2018.
* *7 Joeph Redmon and Ali Farhadi, "YOLO9000: Better, Faster, Stronger, " best paper honorable Mention, CVPR, 2017.
* *8 Joeph Redmon and Ali Farhadi, "You Only Look Once: Unified, Real-Time Object Detection, " Open CV People's Choice Award, CVPR, 2016.
* *9 Sean J. Taylor and Benjamin Letham, "Forcasting at Scale, " Tech report, Facebook, 2017
